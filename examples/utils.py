from datetime import timedelta

import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import geopandas
import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import requests
import seaborn as sns
from cartopy.io import shapereader
from catalog_tools.plots.basics import dot_size
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import FuncFormatter
from shapely.geometry import Polygon

import utils as ut


def get_data(base_url: str, path: str):
    r = requests.get(f'{base_url}{path}')
    return r.json()


def normalize_data(data):
    events = []
    for cat in data:
        for ev in cat['events']:
            ev['catalog_id'] = cat['id']
            events.append(ev)
    df = pd.json_normalize(events, sep='_')

    if 'datetime_value' in df.columns:
        df['datetime_value'] = pd.to_datetime(
            df['datetime_value'], format='ISO8601')

    return df


def bin_data(start, end, data, binsize=24):
    bins = pd.date_range(start, end, freq=f"{binsize}h")
    bins_taken, _ = pd.cut(data, bins=bins, retbins=True)
    return bins_taken


def frequency(data: pd.DataFrame,
              bin_col: str = 'bin',
              freq_col: str = 'magnitude_value'):

    count = data.groupby(bin_col)[freq_col].agg(count='count')
    count['index'] = count.index.map(lambda x: str(x.left))

    return pd.Series(count['count'].values, index=count['index'])


def frequency_dict(data: pd.DataFrame,
                   bin_col: str = 'bin',
                   freq_col: str = 'magnitude_value'):

    count = frequency(*locals().values())
    return pd.DataFrame({'index': count.index,
                         'count': count.values}).to_dict(orient='records')


def frequency_statistics(data: pd.DataFrame,
                         bin_col: str = 'bin',
                         stat_col: str = 'catalog_id',
                         freq_col: str = 'magnitude_value'):

    grouped = data.groupby([bin_col, stat_col])[freq_col].count()

    statistics = grouped.groupby(bin_col).agg(mean='mean')

    statistics['index'] = statistics.index.map(lambda x: str(x.left))
    statistics['qt_5'] = grouped.groupby(bin_col).quantile(0.05)
    statistics['qt_50'] = grouped.groupby(bin_col).quantile(0.50)
    statistics['qt_95'] = grouped.groupby(bin_col).quantile(0.95)

    return statistics.set_index('index')


def frequency_statistics_dict(data: pd.DataFrame,
                              bin_col: str = 'bin',
                              stat_col: str = 'catalog_id',
                              freq_col: str = 'magnitude_value'):

    statistics = ut.frequency_statistics(*locals().values())
    statistics['index'] = statistics.index
    return statistics.to_dict(orient='records')


def rect_from_bound(xmin, xmax, ymin, ymax):
    """Returns list of (x,y)'s for a rectangle"""
    xs = [xmax, xmin, xmin, xmax, xmax]
    ys = [ymax, ymax, ymin, ymin, ymax]
    return [(x, y) for x, y in zip(xs, ys)]


def plot_forecast(
        training_data,
        forecast_data,
        catalog,
        forecast_start,
        outlines,
        country='Switzerland'):

    # Style and functions

    font = {'family': 'sans serif',
            'weight': 'light',
            'size': 14}

    col_bars = '#393E41'
    col_dots = '#F5DF4D'

    matplotlib.rc('font', **font)

    catalog['dot_size'] = dot_size(
        catalog["magnitude_value"],
        smallest=100,
        largest=800,
        interpolation_power=3
    )

    # Geometries and Maps
    resolution = '10m'
    category = 'cultural'
    name = 'admin_0_countries'
    shpfilename = shapereader.natural_earth(resolution, category, name)
    df = geopandas.read_file(shpfilename)

    poly = [df.loc[df['ADMIN'] == country]['geometry'].values[0]]
    stamen_terrain = cimgt.Stamen('terrain-background', desired_tile_form="L")

    # projections that are used
    st_proj = stamen_terrain.crs  # projection used by Stamen images
    ll_proj = ccrs.PlateCarree()  # CRS for raw long/lat

    recti = Polygon(np.flip(outlines))
    exts = [recti.bounds[0], recti.bounds[2], recti.bounds[1], recti.bounds[3]]

    # Plotting

    fig = plt.figure(figsize=(10, 15), linewidth=1)

    plt.set_cmap('Greys')
    ax = {}

    height_ratios = [3, 1, 1]
    wspace = 0.8

    # set up grid
    gs2 = GridSpec(
        nrows=3, ncols=1,
        height_ratios=height_ratios,
        hspace=0.1, wspace=wspace
    )

    # fill grid with subplots
    ax['a'] = plt.subplot(gs2[0], projection=st_proj)
    ax['b'] = plt.subplot(gs2[1])
    ax['c'] = plt.subplot(gs2[2], sharex=ax['b'])

    # adjust padding between subplots
    plt.rcParams["figure.autolayout"] = False
    plt.subplots_adjust(hspace=0.4)
    plt.subplots_adjust(wspace=0.3)

    # disable ticks on b and c and disable axis on a
    plt.setp(ax['b'].get_xticklabels(), visible=False)

    # set rectancle extent of map
    ax['a'].set_extent(exts, crs=ll_proj)

    # Add the Stamen data at zoom level 8.
    ax['a'].add_image(stamen_terrain, 8,
                      cmap=sns.dark_palette(
                          '#7B868C',
                          (0.5, 0.5, 0.6),
                          as_cmap=True, reverse=False
                      ))

    # create mask for greying out areas outside of Switzerland
    msk = Polygon(ut.rect_from_bound(*exts)).difference(poly[0].simplify(0.01))
    msk_stm = st_proj.project_geometry(msk, ll_proj)
    ax['a'].add_geometries(
        msk_stm,
        st_proj,
        zorder=12,
        facecolor='white',
        edgecolor='none',
        alpha=0.65)

    # add country border
    ax['a'].projection = st_proj
    ax['a'].add_geometries(
        poly,
        crs=ll_proj,
        facecolor='none',
        edgecolor='k',
        zorder=13)

    # add observed earthquakes
    ax['a'].scatter(
        catalog.x_value,
        catalog.y_value,
        c=col_dots,
        edgecolor='k',
        s=catalog.dot_size,
        zorder=100,
        transform=ccrs.PlateCarree(),
        linewidth=0.5, alpha=0.8,
        label='observed earthquakes\n since '
        f'{forecast_start.date() - pd.Timedelta("7d")}'
    )

    ax['a'].legend(fancybox=False, loc='upper left').set_zorder(20)

    # add a scatterplot for magnitudes of observed earthquakes
    ax['b'].scatter(
        catalog.datetime_value,
        catalog.magnitude_value,
        c=col_dots,
        edgecolor='k',
        s=catalog.dot_size,
        zorder=100,
        linewidth=0.5, alpha=0.8,
    )

    ax['b'].set_ylabel('Magnitude')
    ax['b'].set_ylim(1, 6.0)

    # add a barplot for number of observed earthquakes
    ax['c'].bar(
        training_data.index,
        training_data,
        color=col_bars,
        label='observed number')
    ax['c'].set_ylabel('Number of M≥2.3\nearthquakes')
    (_, caps, _) = ax['c'].errorbar(
        forecast_data['mean'].index,
        forecast_data['qt_50'],
        yerr=[forecast_data['qt_50'] - forecast_data['qt_5'],
              forecast_data['qt_95'] - forecast_data['qt_50']],
        capsize=5, ecolor='k', linewidth=1, color='none',
        mfc=col_bars, ms=7, marker='D', mec='w',
        label=r'forecast with 90% confidence interval',
    )
    ax['c'].set_ylim(0, forecast_data['qt_95'].max() + 1)
    ax['c'].legend(loc='upper right', fontsize='x-small', frameon=False)
    ax['c'].tick_params(axis='x', rotation=45, labelsize=16)
    ax['c'].set_xlabel("Time")
    ax['c'].text(forecast_start
                 + timedelta(hours=24),
                 (forecast_data['qt_95'].max() + 1) * 0.75,
                 'forecast date',
                 rotation=90,
                 ha='right',
                 va='center',
                 fontsize=10)

    # Set the xticklabels format
    xfmt = mdates.DateFormatter("%d.%m.")
    ax['c'].xaxis.set_major_formatter(xfmt)

    # draw vertical line for forecast start
    for panel in ['b', 'c']:
        ax[panel].axvline(
            forecast_start.replace(
                hour=0),
            linestyle='--',
            color=col_bars,
            linewidth=1)

    return fig


def plot_probabilities(
        forecast_data, mc,
        forecast_start,
        beta):
    # Style and functions
    font = {'family': 'sans serif',
            'weight': 'light',
            'size': 12}
    col_forecast = '#00ADB5'  # '#FFA07A'  # '#798280'
    col_bars = '#393E41'

    matplotlib.rc('font', **font)

    fig, ax = plt.subplots(figsize=(10, 3))

    plt.rcParams["figure.autolayout"] = False
    plt.subplots_adjust(hspace=0.1)

    ax.set_ylabel('Daily Earthquake\nProbability')
    ax.plot(forecast_data['mean'].index,
            1 - np.power(1 - np.exp(-beta * (2.5 - mc)),
                         forecast_data['mean']),
            c=col_bars,
            label='M≥2.5',
            marker='.')

    ax.plot(forecast_data['mean'].index,
            1 - np.power(1 - np.exp(-beta * (5 - mc)),
                         forecast_data['mean']),
            c=col_forecast,
            label='M≥5',
            marker='.')

    def y_fmt(y, _):
        if 100 * y >= 1:
            return f"{100*y:.0f}%"
        elif y < 1:
            return f"{100*y:.2}%"

    ax.legend(loc='upper right', frameon=True, )
    ax.set_yscale("log")
    ax.yaxis.set_major_formatter(FuncFormatter(y_fmt))
    ax.set_yticks([0.00001, 0.0001, 0.001, 0.01, 0.1, 0.5])
    ax.axvline(
        forecast_start.replace(
            hour=0),
        linestyle='--',
        color=col_bars,
        linewidth=1)
    ymin, ymax = ax.get_ylim()
    ymid = ymin * np.power(ymax / ymin, 1 / 2)

    ax.text(
        forecast_start.replace(
            hour=0),
        ymid,
        'forecast date',
        rotation=90,
        ha='right',
        va='center',
        fontsize=10)

    ax.tick_params(axis='x', rotation=45, labelsize=16)
    ax.set_xlabel("Time")
    xfmt = mdates.DateFormatter("%d.%m.")
    ax.xaxis.set_major_formatter(xfmt)

    return fig


def plot_probabilities_comparison(
        forecast_data1, mc1,
        forecast_data2, mc2,
        forecast_start,
        beta):
    # Style and functions
    font = {'family': 'sans serif',
            'weight': 'light',
            'size': 12}
    col_forecast = '#00ADB5'  # '#FFA07A'  # '#798280'
    col_bars = '#393E41'

    matplotlib.rc('font', **font)

    fig, (ax1, ax2) = plt.subplots(
        nrows=2, ncols=1, figsize=(10, 6), sharex=True)

    plt.rcParams["figure.autolayout"] = False
    plt.subplots_adjust(hspace=0.1)

    ax1.set_ylabel('Daily Earthquake\nProbability\nM≥2.5')
    ax1.plot(forecast_data1['mean'].index,
             1 - np.power(1 - np.exp(-beta * (2.5 - mc1)),
                          forecast_data1['mean']),
             c=col_bars,
             label='Model 1',
             marker='.')

    ax1.plot(forecast_data2['mean'].index,
             1 - np.power(1 - np.exp(-beta * (2.5 - mc2)),
                          forecast_data2['mean']),
             c=col_forecast,
             label='Model 2',
             marker='.')

    ax2.set_ylabel('Daily Earthquake\nProbability\nM≥5.0')
    ax2.plot(forecast_data1['mean'].index,
             1 - np.power(1 - np.exp(-beta * (5.0 - mc1)),
                          forecast_data1['mean']),
             c=col_bars,
             label='Model 1',
             marker='.')

    ax2.plot(forecast_data2['mean'].index,
             1 - np.power(1 - np.exp(-beta * (5.0 - mc2)),
                          forecast_data2['mean']),
             c=col_forecast,
             label='Model 2',
             marker='.')

    def y_fmt(y, _):
        if 100 * y >= 1:
            return f"{100*y:.0f}%"
        elif y < 1:
            return f"{100*y:.2}%"

    for ax in [ax1, ax2]:
        ax.legend(loc='upper right', frameon=True, )
        ax.set_yscale("log")
        ax.yaxis.set_major_formatter(FuncFormatter(y_fmt))
        ax.set_yticks([0.00001, 0.0001, 0.001, 0.01, 0.1, 0.5])
        ax.axvline(
            forecast_start.replace(
                hour=0),
            linestyle='--',
            color=col_bars,
            linewidth=1)
        ymin, ymax = ax.get_ylim()
        ymid = ymin * np.power(ymax / ymin, 1 / 2)

        ax.text(
            forecast_start.replace(
                hour=0),
            ymid,
            'forecast date',
            rotation=90,
            ha='right',
            va='center',
            fontsize=10)

    ax2.tick_params(axis='x', rotation=45, labelsize=16)
    ax2.set_xlabel("Time")
    xfmt = mdates.DateFormatter("%d.%m.")
    ax2.xaxis.set_major_formatter(xfmt)

    return fig
