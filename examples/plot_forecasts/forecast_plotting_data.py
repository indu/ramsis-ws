import datetime as dt
import json

import pandas as pd
import requests


def get_data(base_url: str, path: str):
    r = requests.get(f'{base_url}{path}')
    return r.json()


def normalize_data(data):
    events = []
    for cat in data:
        for ev in cat['events']:
            ev['catalog_id'] = cat['id']
            events.append(ev)
    df = pd.json_normalize(events, sep='_')

    if 'datetime_value' in df.columns:
        df['datetime_value'] = pd.to_datetime(
            df['datetime_value'], format='ISO8601')

    return df


def bin_data(start, end, data, binsize=24):
    bins = pd.date_range(start, end, freq=f"{binsize}h")
    bins_taken, _ = pd.cut(data, bins=bins, retbins=True)
    return bins_taken


def frequency_dict(data: pd.DataFrame,
                   bin_col: str = 'bin',
                   freq_col: str = 'magnitude_value'):

    count = data.groupby(bin_col)[freq_col].agg(count='count')
    count['index'] = count.index.map(lambda x: str(x.left))

    return count.to_dict(orient='records')


def frequency_statistics_dict(data: pd.DataFrame,
                              bin_col: str = 'bin',
                              stat_col: str = 'catalog_id',
                              freq_col: str = 'magnitude_value'):

    grouped = data.groupby([bin_col, stat_col])[freq_col].count()

    statistics = grouped.groupby(bin_col).agg(mean='mean')

    statistics['index'] = statistics.index.map(lambda x: str(x.left))
    statistics['qt_5'] = grouped.groupby(bin_col).quantile(0.05)
    statistics['qt_50'] = grouped.groupby(bin_col).quantile(0.50)
    statistics['qt_95'] = grouped.groupby(bin_col).quantile(0.95)

    return statistics.to_dict(orient='records')


if __name__ == '__main__':
    url = 'http://ramsis-rise.ethz.ch:8000/v1'
    forecast = get_data(url, '/forecasts/1')
    catalog = get_data(url, '/forecasts/1/seismiccatalog')
    modelrun = get_data(url, '/modelruns/1')
    data = get_data(url, '/modelruns/1/results')

    simulations = normalize_data(data[0]['spatialbins'][0]['catalogs'])
    catalog = normalize_data([catalog])

    # read the true catalog
    # catalog = pd.read_csv("ch_catalog.csv", parse_dates=["time"])
    # simulations = pd.read_csv("simulations_ch.csv", parse_dates=["time"])

    # start time of the forecast
    fc_time = pd.to_datetime(forecast['starttime'])

    # how long one bin should be in hours in the plotted time series
    hour_bins = 24

    # mc comes from the forecast config/output.
    mc = modelrun['modelconfig']['config']['model_parameters']['mc']

    # beta is one of the estimated parameters
    beta = 2.422995571846904

    simulations['bin'] = bin_data(
        fc_time,
        fc_time
        + dt.timedelta(
            days=30.1),
        simulations['datetime_value'])

    catalog['bin'] = \
        bin_data(fc_time - dt.timedelta(days=7),
                 fc_time + dt.timedelta(days=0.1), catalog['datetime_value'])

    catalog['bin_ev'] = \
        bin_data(fc_time,
                 fc_time + dt.timedelta(days=30.1),
                 catalog['datetime_value'])

    train_data = frequency_dict(
        catalog.query("datetime_value <= @fc_time and magnitude_value >= @mc"))

    observed_data = frequency_dict(catalog, bin_col='bin_ev')

    forecast_data = frequency_statistics_dict(simulations)

    with open('plot_train_data.json', 'w') as f:
        f.write(json.dumps(train_data))

    with open('plot_observed_data.json', 'w') as f:
        f.write(json.dumps(observed_data))

    with open('plot_forecast_data.json', 'w') as f:
        f.write(json.dumps(forecast_data))
