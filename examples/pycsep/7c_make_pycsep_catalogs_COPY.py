import sys

# import numpy as np
import pandas as pd

if sys.platform == 'linux':
    sim_path = '/cluster/scratch/mizrahil/oef_ch/extra_simulations/'
    n_simulations_overall = 100000
    n_files = 20

else:
    sim_path = '/Volumes/LaCie/oef/extra_simulations/'
    n_simulations_overall = 1000
    n_files = 1

# doing these tests for different versions (modes) of the model.
modes = [
    "const",
    "mcvar",
    "hazbg",
    "hazbval",
    "hazall",
    "cali"
]

for mode in modes:
    if mode == 'mcvar':
        aux_y = '_1972'
    else:
        aux_y = '_1992'
    dfs = []

    # looping through all files that contain simulations for this mode, and
    # turning them into a different format..
    for file_i in range(n_files):
        fnfn = sim_path + "sim_2021-12-26_" + mode + aux_y + \
            "_18250D_file_" + str(file_i) + "_csep.csv"
        df = pd.read_csv(
            fnfn,
            # index_col=[0],
            # header=None,
            # parse_dates=["time"],
        )
        dfs.append(df)

    df = pd.concat(dfs)

    if len(df.query("catalog_id == @n_simulations_overall-1")) == 0:
        try:
            df.iloc[len(df), :] = [None, None, None, None,
                                   n_simulations_overall - 1, None]
        except BaseException:
            print("couldn't add last catalog, have to do this when evaluating")

    df["catalog_id"] = df["catalog_id"].astype(int)

    df.to_csv(
        sim_path
        + "sim_1e5_50y_pyCSEPformat_"
        + mode
        + ".csv",
        index=False)
