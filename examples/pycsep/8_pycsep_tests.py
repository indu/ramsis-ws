import datetime as dt
import json
from itertools import product

import csep
import matplotlib.pyplot as plt
import numpy as np
from csep.core import regions
from csep.utils import time_utils

fc_dayss = [30 * 365, 50 * 365]
modes = [
    "const",
    "mcvar",
    "hazbg",
    "hazbval",
    "hazall",
    "cali",
    "source"
]
min_mws = [4.45]
combos = list(product(min_mws, fc_dayss, modes))
print(len(combos))


for run_number in range(0, 25, 1):

    min_mw, fc_days, mode = combos[run_number]
    print("min_mw", min_mw, "fc_days", fc_days, "mode", mode)

    start_time = time_utils.strptime_to_utc_datetime("2021-12-26 00:00:00.0")
    end_time = start_time + dt.timedelta(fc_days)

    # Magnitude bins properties
    min_mw = min_mw
    max_mw = 11.05
    dmw = 0.1

    # Create space and magnitude regions
    magnitudes = regions.magnitude_bins(min_mw, max_mw, dmw)
    region = csep.core.regions.CartesianGrid2D.from_origins(
        np.array(list(product(
            np.arange(5.85, 10.61, 0.05), np.arange(45.7, 47.91, 0.05)
        )))
    )

    # Bind region information to the forecast (this will be used for binning
    # of the catalogs)
    space_magnitude_region = regions.create_space_magnitude_region(
        region, magnitudes)

    # define forecast
    forecast = csep.load_catalog_forecast(
        "/Volumes/LaCie/oef/extra_simulations/sim_1e5_50y_pyCSEPformat_"
        + mode
        + ".csv",
        start_time=start_time,
        end_time=end_time,
        region=space_magnitude_region,
        apply_filters=True)
    forecast.filters = [
        f'origin_time >= {forecast.start_epoch}',
        f'origin_time < {forecast.end_epoch}',
        f'magnitude >= {forecast.min_magnitude}']

    # define period in which to check observed catalog
    end_time_obs = time_utils.strptime_to_utc_datetime("1992-01-01 00:00:00.0")
    start_time_obs = end_time_obs - dt.timedelta(fc_days)

    dummy_forecast = csep.load_catalog_forecast(
        # datasets.ucerf3_ascii_format_landers_fname
        "/Volumes/LaCie/oef/extra_simulations/" \
        "sim_2021-12-26_const_1992_18250D_file_0_csep.csv",
        start_time=start_time_obs, end_time=end_time_obs,
        region=space_magnitude_region,
        apply_filters=True
    )
    dummy_forecast.filters = [
        f'origin_time >= {dummy_forecast.start_epoch}',
        f'origin_time < {dummy_forecast.end_epoch}']

    # define observed SED catalog
    ch_cat = csep.load_catalog("../data/scsep_ch.csv")
    ch_cat.name = "SED"

    ch_cat = ch_cat.filter_spatial(forecast.region)
    ch_cat = ch_cat.filter(f'magnitude >= {min_mw}')
    ch_cat = ch_cat.filter(dummy_forecast.filters)

    fn_result = "../data/cseptests/tests_" + mode + \
        "_fcdays_" + str(fc_days) + "_mmin_" + str(min_mw) + "_"
    # TESTS
    _ = forecast.get_expected_rates(verbose=True, )
    number_test_result = csep.core.catalog_evaluations.number_test(
        forecast, ch_cat)
    result_json = json.dumps(number_test_result.to_dict())
    with open(fn_result + "number.json", "w") as f:
        f.write(result_json)
    spatial_test_result = csep.core.catalog_evaluations.spatial_test(
        forecast, ch_cat)
    result_json = json.dumps(spatial_test_result.to_dict())
    with open(fn_result + "spatial.json", "w") as f:
        f.write(result_json)
    magnitude_test_result = csep.core.catalog_evaluations.magnitude_test(
        forecast, ch_cat)
    result_json = json.dumps(magnitude_test_result.to_dict())
    with open(fn_result + "magnitude.json", "w") as f:
        f.write(result_json)
    pseudolikelihood_test_result = csep.core.catalog_evaluations\
        .pseudolikelihood_test(forecast, ch_cat)
    result_json = json.dumps(pseudolikelihood_test_result.to_dict())
    with open(fn_result + "pseudolikelihood.json", "w") as f:
        f.write(result_json)

    fig, axes = plt.subplots(nrows=2, ncols=2)
    axes[0][0] = number_test_result.plot(show=False)
    plt.savefig(
        "../plots/cseptests/tests_"
        + mode
        + "_fcdays_"
        + str(fc_days)
        + "_mmin_"
        + str(min_mw)
        + "_number.pdf",
        bbox_inches='tight',
        dpi=100)
    axes[0][1] = spatial_test_result.plot(show=False)
    plt.savefig(
        "../plots/cseptests/tests_"
        + mode
        + "_fcdays_"
        + str(fc_days)
        + "_mmin_"
        + str(min_mw)
        + "_spatial.pdf",
        bbox_inches='tight',
        dpi=100)
    axes[1][0] = magnitude_test_result.plot(show=False, plot_args={'bins': 20})
    plt.savefig(
        "../plots/cseptests/tests_"
        + mode
        + "_fcdays_"
        + str(fc_days)
        + "_mmin_"
        + str(min_mw)
        + "_magnitude.pdf",
        bbox_inches='tight',
        dpi=100)
    axes[1][1] = pseudolikelihood_test_result.plot(show=False)
    plt.savefig(
        "../plots/cseptests/tests_"
        + mode
        + "_fcdays_"
        + str(fc_days)
        + "_mmin_"
        + str(min_mw)
        + "_pseudolikelihood.pdf",
        bbox_inches='tight',
        dpi=100)
