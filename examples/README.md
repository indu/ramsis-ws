# OEF Switzerland

The data from the operational earthquake forecasts in Switzerland can be explored and visualized using the Jupyter Notebook `oef_switzerland.ipynb` in this folder.

To install you can run the following commands:

```bash
# clone the repo and move to this current folder
git clone https://gitlab.seismo.ethz.ch/indu/ramsis-ws.git
cd examples

# the notebooks require python 3.10. If this is your standard version you can use:
python3 -m venv env

# else you need to install python3.10 and then run
python3.10 -m venv env

# update and install the necessary libraries
source env/bin/activate
pip install -U pip wheel setuptools
pip install -r requirements.txt

# run jupyter and then open the notebook in the browser
jupyter notebook
```
