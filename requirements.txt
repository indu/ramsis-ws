asyncpg>=0.29.0
fastapi[all]>=0.110.0
geoalchemy2>=0.14.6
gunicorn>=21.2.0
pydantic>=2.6.3
pydantic_settings>=2.2.1
ramsis.datamodel @ git+https://gitlab.seismo.ethz.ch/indu/ramsis.datamodel.git
shapely>=2.0.3
SQLAlchemy>=2.0.27
uvicorn>=0.27.1
