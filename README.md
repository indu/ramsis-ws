# ramsis-ws

[![pipeline status](https://gitlab.seismo.ethz.ch/indu/ramsis-ws/badges/develop/pipeline.svg)](https://gitlab.seismo.ethz.ch/indu/ramsis-ws/-/commits/develop)
[![coverage report](https://gitlab.seismo.ethz.ch/indu/ramsis-ws/badges/develop/coverage.svg)](https://gitlab.seismo.ethz.ch/indu/ramsis-ws/-/commits/develop)

Web service for access to data defined by ramsis.datamodel
