from datetime import datetime

from ramsis.datamodel import (Forecast, ForecastSeries, InjectionPlan,
                              ModelConfig, ModelRun, Project, ResultTimeBin,
                              SeismicForecastGrid, Tag)
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import defer, joinedload


async def read_all_projects(db: AsyncSession,
                            starttime: datetime | None = None,
                            with_input_data: bool = False) \
        -> list[Project]:

    statement = select(Project)

    if not with_input_data:
        statement = statement.options(
            defer(Project.seismiccatalog, raiseload=True),
            defer(Project.injectionwell, raiseload=True))

    if starttime:
        statement = statement.filter(Project.starttime > starttime)

    results = await db.execute(statement)

    return results.scalars().unique().all()


async def read_project(db: AsyncSession,
                       project_id: int,
                       with_input_data: bool = False) -> \
        Project | None:

    statement = select(Project).where(Project.id == project_id)

    if not with_input_data:
        statement = statement.options(
            defer(Project.seismiccatalog, raiseload=True),
            defer(Project.injectionwell, raiseload=True))

    result = await db.execute(statement)

    return result.scalars().unique().one_or_none()


async def read_all_forecastseries(db: AsyncSession, project_id: int) \
        -> list[ForecastSeries]:

    statement = select(ForecastSeries) \
        .options(joinedload(ForecastSeries.tags),
                 joinedload(ForecastSeries.injectionplans),
                 defer(ForecastSeries.log, raiseload=True),) \
        .where(project_id == project_id)

    results = await db.execute(statement)

    return results.scalars().unique().all()


async def read_forecastseries(db: AsyncSession,
                              forecastseries_id: int) \
        -> ForecastSeries | None:

    statement = select(ForecastSeries) \
        .options(joinedload(ForecastSeries.tags),
                 joinedload(ForecastSeries.injectionplans),
                 defer(ForecastSeries.log, raiseload=True),) \
        .where(ForecastSeries.id == forecastseries_id)

    result = await db.execute(statement)

    return result.scalars().unique().one_or_none()


async def read_modelconfigs(db: AsyncSession,
                            tag_names: list[int]) \
        -> list[ModelConfig]:
    statement = select(ModelConfig).where(
        ModelConfig.tags.any(Tag.name.in_(tag_names)))

    results = await db.execute(statement)

    return results.scalars().unique()


async def read_forecastseries_modelconfigs(db: AsyncSession,
                                           forecastseries_id: int) \
        -> list[ModelConfig]:

    statement = select(ForecastSeries) \
        .options(joinedload(ForecastSeries.tags),
                 defer(ForecastSeries.log, raiseload=True),) \
        .where(ForecastSeries.id == forecastseries_id)

    result = await db.execute(statement)
    result = result.scalar()

    if not result:
        return None

    tag_names = [tag.name for tag in result.tags]

    result = await read_modelconfigs(db, tag_names)

    return result


async def read_all_forecasts(db: AsyncSession, forecastseries_id: int) \
        -> list[Forecast]:

    statement = select(Forecast).options(defer(Forecast.log, raiseload=True)) \
        .where(Forecast.forecastseries_id == forecastseries_id)

    results = await db.execute(statement)

    return results.scalars().unique().all()


async def read_forecast(db: AsyncSession, forecast_id: int):

    statement = select(Forecast).options(
        defer(Forecast.log, raiseload=True),
    ).where(Forecast.id == forecast_id)

    result = await db.execute(statement)

    return result.scalars().unique().one_or_none()


async def read_forecast_modelruns(db: AsyncSession, forecast_id: int):

    # load Forecast, defer well&catalog
    # join model runs, defer injectionplan
    # subqueryload modelconfig, load only name
    statement = select(Forecast) \
        .options(
            defer(Forecast.injectionwell, raiseload=True),
            defer(Forecast.seismiccatalog, raiseload=True),
            defer(Forecast.log, raiseload=True),
            joinedload(Forecast.runs)
            .subqueryload(ModelRun.modelconfig)
            .load_only(ModelConfig.name, ModelConfig.id),
            joinedload(Forecast.runs)
            .subqueryload(ModelRun.injectionplan)
            .load_only(InjectionPlan.name, InjectionPlan.id)
    ) \
        .where(Forecast.id == forecast_id)

    result = await db.execute(statement)

    return result.scalar()


async def read_forecast_rates(db: AsyncSession,
                              forecast_id: int,
                              modelconfigs: list[str] | None = None,
                              injectionplans: list[str] | None = None):

    statement = select(ModelRun) \
        .options(defer(ModelRun.log, raiseload=True),
                 joinedload(ModelRun.modelconfig)
                 .load_only(ModelConfig.name, ModelConfig.id),
                 joinedload(ModelRun.injectionplan)
                 .load_only(InjectionPlan.name, InjectionPlan.id),
                 joinedload(ModelRun.resulttimebins)
                 .subqueryload(ResultTimeBin.seismicforecastgrids)
                 .subqueryload(SeismicForecastGrid.seismicrates)
                 ) \
        .where(ModelRun.forecast_id == forecast_id)

    if modelconfigs:
        statement = statement.join(ModelConfig).where(
            ModelConfig.name.in_(modelconfigs))

    if injectionplans:
        statement = statement.join(InjectionPlan).where(
            InjectionPlan.name.in_(injectionplans))

    result = await db.execute(statement)

    return result.scalars().unique()


async def read_forecast_injectionwells(db: AsyncSession, forecast_id: int):

    statement = select(Forecast.injectionwell).where(Forecast.id == forecast_id)

    result = await db.execute(statement)

    return result.scalar()


async def read_forecast_seismiccatalog(db: AsyncSession, forecast_id: int):

    statement = select(
        Forecast.seismiccatalog).where(
        Forecast.id == forecast_id)

    result = await db.execute(statement)

    return result.scalar()


async def read_injectionplan(db: AsyncSession, injectionplan_id: int):

    statement = select(InjectionPlan.data).where(
        InjectionPlan.id == injectionplan_id)

    result = await db.execute(statement)

    return result.scalar()


async def read_all_modelconfigs(db: AsyncSession):

    statement = select(ModelConfig)

    result = await db.execute(statement)

    return result.scalars().unique().all()


async def read_modelconfig(db: AsyncSession, modelconfig_id: int):

    statement = select(ModelConfig).where(
        ModelConfig.id == modelconfig_id)

    result = await db.execute(statement)

    return result.scalar()


async def read_modelrun_rates(db: AsyncSession, modelrun_id: int):

    statement = select(ModelRun) \
        .options(defer(ModelRun.log, raiseload=True),
                 joinedload(ModelRun.modelconfig)
                 .load_only(ModelConfig.name, ModelConfig.id),
                 joinedload(ModelRun.injectionplan)
                 .load_only(InjectionPlan.name, InjectionPlan.id),
                 joinedload(ModelRun.resulttimebins)
                 .subqueryload(ResultTimeBin.seismicforecastgrids)
                 .subqueryload(SeismicForecastGrid.seismicrates)
                 ) \
        .where(ModelRun.id == modelrun_id)

    result = await db.execute(statement)

    return result.scalar()


async def read_modelrun_modelconfig(db: AsyncSession, modelrun_id: int):

    statement = select(ModelConfig) \
        .join(ModelRun, ModelRun.modelconfig_id == ModelConfig.id) \
        .where(ModelRun.id == modelrun_id)

    result = await db.execute(statement)

    return result.scalar()


async def read_forecastseries_injectionplans(
        db: AsyncSession,
        forecastseries_id: int) -> list[InjectionPlan]:
    statement = select(InjectionPlan) \
        .where(InjectionPlan.forecastseries_id == forecastseries_id)

    result = await db.execute(statement)

    return result.scalars().unique()
