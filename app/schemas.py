
import json
from datetime import datetime

from pydantic import Field, computed_field, field_validator

from app.mixins import (CreationInfoMixin, Model, create_validator, get_input,
                        get_status, real_float_value_mixin)


class ProjectSchema(CreationInfoMixin):
    id: int
    name: str | None
    description: str | None
    fdsnws_url: str | None
    hydws_url: str | None
    seismiccatalog_required: str
    injectionwell_required: str
    injectionplan_required: str
    starttime: datetime | None
    endtime: datetime | None

    _get_catalog_input: classmethod = create_validator(
        'seismiccatalog_required', get_input)
    _get_well_input: classmethod = create_validator(
        'injectionwell_required', get_input)
    _get_plan_input: classmethod = create_validator(
        'injectionplan_required', get_input)


class InjectionPlanNameSchema(Model):
    name: str | None
    id: int


class InjectionPlanSchema(InjectionPlanNameSchema):
    borehole_hydraulics: dict | None = Field(validation_alias="data")

    @field_validator('borehole_hydraulics', mode='before')
    @classmethod
    def load_data(cls, v: str) -> dict:
        return json.loads(v)


class ModelConfigNameSchema(Model):
    name: str | None
    id: int


class ModelConfigSchema(ModelConfigNameSchema):
    url: str | None
    description: str | None
    enabled: bool | None
    result_type: str | None
    sfm_module: str | None
    last_updated: datetime | None
    config: dict | None

    _get_result_type: classmethod = create_validator('result_type', get_input)


class ForecastSeriesSchema(CreationInfoMixin):
    id: int
    starttime: datetime | None
    endtime: datetime | None
    name: str | None
    forecastinterval: float | None
    forecastduration: float | None
    status: str | None
    active: bool | None
    boundingpolygon: str | None
    altitudemin: float | None
    altitudemax: float | None

    modelconfigs: list[ModelConfigNameSchema] | None = None
    injectionplans: list[InjectionPlanNameSchema] | None

    _get_status: classmethod = create_validator('status', get_status)


class ModelRunSchema(Model):
    id: int
    status: str | None
    modelconfig: ModelConfigNameSchema | None = \
        Field(default=None, exclude=True)
    injectionplan: InjectionPlanNameSchema | None = \
        Field(default=None, exclude=True)

    _get_status: classmethod = create_validator('status', get_status)

    @computed_field
    @property
    def modelconfig_name(self) -> str:
        return self.modelconfig.name

    @computed_field
    @property
    def modelconfig_id(self) -> int:
        return self.modelconfig.id

    @computed_field
    @property
    def injectionplan_name(self) -> str:
        return self.injectionplan.name

    @computed_field
    @property
    def injectionplan_id(self) -> int:
        return self.injectionplan.id


class ForecastSchema(CreationInfoMixin):
    id: int
    name: str | None
    starttime: datetime | None
    endtime: datetime | None
    status: str | None

    _get_status: classmethod = create_validator('status', get_status)


class ForecastDetailSchema(ForecastSchema):
    modelruns: list[ModelRunSchema] | None = Field(validation_alias="runs")


class ForecastRateSchema(real_float_value_mixin('b', float),
                         real_float_value_mixin('numberevents', float),
                         real_float_value_mixin('a', float),
                         real_float_value_mixin('alpha', float),
                         real_float_value_mixin('mc', float)):

    latitude_min: float | None = None
    latitude_max: float | None = None
    longitude_min: float | None = None
    longitude_max: float | None = None
    altitude_min: float | None = None
    altitude_max: float | None = None

    grid_id: int | None = Field(validation_alias="seismicforecastgrid_id")


class SeismicForecastGridSchema(Model):
    resulttimebin_id: int
    seismicrates: list[ForecastRateSchema]


class ResultTimeBinSchema(Model):
    starttime: datetime
    endtime: datetime
    seismicforecastgrids: list[SeismicForecastGridSchema] | None = Field(
        default=None, exclude=True)

    @computed_field
    @property
    def rates(self) -> list[ForecastRateSchema]:
        rates = []
        for grid in self.seismicforecastgrids:
            for rate in grid.seismicrates:
                rates.append(rate)
        return rates


class ModelRunRateGridSchema(ModelRunSchema):
    rateforecasts: list[ResultTimeBinSchema] | None = Field(
        validation_alias="resulttimebins")
