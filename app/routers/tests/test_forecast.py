from fastapi.testclient import TestClient

from app.main import app

client = TestClient(app)


def test_read_forecasts():
    response = client.get("/v1/forecast/")
    assert response.status_code == 200


def test_read_forecast():
    response = client.get("/v1/forecast/3")
    assert response.status_code == 200


def test_read_forecast_scenarios():
    response = client.get("/v1/forecast/3/scenario")
    assert response.status_code == 200


def test_read_forecast_catalog():
    response = client.get("/v1/forecast/3/catalog")
    assert response.status_code == 200


def test_read_forecast_well():
    response = client.get("/v1/forecast/3/well")
    assert response.status_code == 200
