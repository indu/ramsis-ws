from fastapi.testclient import TestClient

from app.main import app

client = TestClient(app)


def test_read_projects():
    response = client.get("/v1/project/")
    assert response.status_code == 200


def test_read_project():
    response = client.get("/v1/project/1")
    assert response.status_code == 200


def test_read_project_forecasts():
    response = client.get("/v1/project/1/forecast")
    assert response.status_code == 200
