from fastapi import APIRouter, HTTPException

from app import crud
from app.database import DBSessionDep
from app.schemas import (ForecastSeriesSchema, InjectionPlanSchema,
                         ModelConfigNameSchema, ModelConfigSchema)

router = APIRouter(tags=['forecastseries'])


@router.get("/projects/{project_id}/forecastseries",
            response_model=list[ForecastSeriesSchema],
            response_model_exclude_none=True)
async def get_all_forecastseries(db: DBSessionDep,
                                 project_id: int):
    """
    Returns a list of ForecastSeries
    """

    db_result = await crud.read_all_forecastseries(db, project_id)

    for fc in db_result:
        tags_list = [tag.name for tag in fc.tags]

        model_configs = await crud.read_modelconfigs(
            db, tags_list)

        modelconfigs = [ModelConfigNameSchema.model_validate(
            model) for model in model_configs]

        fc.modelconfigs = modelconfigs

    if not db_result:
        raise HTTPException(status_code=404, detail="No forecastseries found.")

    return db_result


@router.get("/forecastseries/{forecastseries_id}",
            response_model=ForecastSeriesSchema,
            response_model_exclude_none=True)
async def get_forecastseries(db: DBSessionDep,
                             forecastseries_id: int):
    """
    Returns a ForecastSeries
    """

    db_result = await crud.read_forecastseries(db, forecastseries_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No forecastseries found.")

    tags_list = [tag.name for tag in db_result.tags]

    model_configs = await crud.read_modelconfigs(db, tags_list)

    modelconfigs = [ModelConfigNameSchema.model_validate(
        model) for model in model_configs]

    db_result.modelconfigs = modelconfigs

    return db_result


@router.get("/forecastseries/{forecastseries_id}/injectionplans",
            response_model=list[InjectionPlanSchema],
            response_model_exclude_none=True)
async def get_forecastseries_injectionplans(db: DBSessionDep,
                                            forecastseries_id: int):
    """
    Returns a list of InjectionPlans
    """

    db_result = await crud.read_forecastseries_injectionplans(
        db, forecastseries_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No forecastseries found.")

    return db_result


@router.get("/forecastseries/{forecastseries_id}/modelconfigs",
            response_model=list[ModelConfigSchema],
            response_model_exclude_none=True)
async def get_forecastseries_modelconfigs(db: DBSessionDep,
                                          forecastseries_id: int):
    """
    Returns a list of ModelConfigs
    """

    db_result = await crud.read_forecastseries_modelconfigs(
        db, forecastseries_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No forecastseries found.")

    return db_result
