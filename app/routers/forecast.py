from typing import Annotated

from fastapi import APIRouter, HTTPException, Query, Response

from app import crud
from app.database import DBSessionDep
from app.routers import XMLResponse
from app.schemas import (ForecastDetailSchema, ForecastSchema,
                         ModelConfigSchema, ModelRunRateGridSchema)

router = APIRouter(tags=['forecast'])


@router.get("/forecastseries/{forecastseries_id}/forecasts",
            response_model=list[ForecastSchema],
            response_model_exclude_none=False)
async def get_all_forecasts(db: DBSessionDep,
                            forecastseries_id: int):
    """
    Returns a list of ForecastSeries
    """

    db_result = await crud.read_all_forecasts(db, forecastseries_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No forecastseries found.")

    return db_result


@router.get("/forecasts/{forecast_id}",
            response_model=ForecastDetailSchema,
            response_model_exclude_none=False)
async def get_forecast(db: DBSessionDep,
                       forecast_id: int):
    """
    Returns a single Forecast
    """

    db_result = await crud.read_forecast_modelruns(db, forecast_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No forecast found.")

    return db_result


@router.get("/forecasts/{forecast_id}/rates",
            response_model=list[ModelRunRateGridSchema],
            response_model_exclude_none=True)
async def get_forecast_rates(
        db: DBSessionDep,
        forecast_id: int,
        modelconfigs: Annotated[list[str] | None, Query()] = None,
        injectionplans: Annotated[list[str] | None, Query()] = None):
    """
    Returns a list of ForecastRateGrids
    """
    db_result = await crud.read_forecast_rates(db,
                                               forecast_id,
                                               modelconfigs,
                                               injectionplans)

    if not db_result:
        raise HTTPException(status_code=404, detail="No forecast found.")

    return db_result


@router.get("/forecasts/{forecast_id}/seismiccatalog",
            responses={
                200: {
                    "content": {"application/xml": {}},
                    "description": "Return the seismic catalog as QML.",
                }
            },
            response_class=XMLResponse)
async def get_forecast_seismiccatalog(db: DBSessionDep,
                                      forecast_id: int):
    """
    Returns the seismic catalog for this project.
    """
    db_result = await crud.read_forecast_seismiccatalog(db, forecast_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No projects found.")

    return Response(
        content=db_result,
        media_type="application/xml")


@router.get("/forecasts/{forecast_id}/injectionwells",
            responses={
                200: {
                    "content": {"application/json": {}},
                    "description": "Return the HYDWS JSON.",
                }
            })
async def get_forecast_injectionwell(db: DBSessionDep, forecast_id: int):

    db_result = await crud.read_forecast_injectionwells(db, forecast_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No forecast found.")

    return Response(
        content=db_result,
        media_type="application/json")


@router.get("/injectionplans/{injectionplan_id}",
            responses={
                200: {
                    "content": {"application/json": {}},
                    "description": "Return the injection plan as JSON.",
                }
            })
async def get_injectionplan(db: DBSessionDep, injectionplan_id: int):

    db_result = await crud.read_injectionplan(db, injectionplan_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No injection plan found.")

    return Response(
        content=db_result,
        media_type="application/json")


@router.get("/modelconfigs",
            response_model=list[ModelConfigSchema],
            response_model_exclude_none=False)
async def get_all_modelconfigs(db: DBSessionDep):

    db_result = await crud.read_all_modelconfigs(db)

    if not db_result:
        raise HTTPException(status_code=404, detail="No model configs found.")

    return db_result


@router.get("/modelconfigs/{modelconfig_id}",
            response_model=ModelConfigSchema,
            response_model_exclude_none=False)
async def get_modelconfig(db: DBSessionDep, modelconfig_id: int):

    db_result = await crud.read_modelconfig(db, modelconfig_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No model config found.")

    return db_result


@router.get("/modelruns/{modelrun_id}/rates",
            response_model=ModelRunRateGridSchema,
            response_model_exclude_none=True)
async def get_modelrun_rates(db: DBSessionDep, modelrun_id: int):
    db_result = await crud.read_modelrun_rates(db,
                                               modelrun_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No modelrun found.")

    return db_result


@router.get("/modelruns/{modelrun_id}/modelconfig",
            response_model=ModelConfigSchema,
            response_model_exclude_none=False)
async def get_modelrun_modelconfig(db: DBSessionDep, modelrun_id: int):
    db_result = await crud.read_modelrun_modelconfig(db,
                                                     modelrun_id)

    if not db_result:
        raise HTTPException(status_code=404, detail="No modelrun found.")

    return db_result
